// package oob examples
package oob

import "fmt"

func ExampleMapStringArr() {
	arr := []string{"a", "b", "c"}

	m := MapStringArr(arr)

	fmt.Println(m[1])
	if v, ok := m[1]; ok {
		fmt.Println(v)
	} else {
		fmt.Println("Index out of bounds")
	}

	fmt.Println(m[4])
	if v, ok := m[4]; ok {
		fmt.Println(v)
	} else {
		fmt.Println("Index out of bounds")
	}

	// Output:
	// b
	// b
	// 
	// Index out of bounds

}

func ExampleStrings() {
	arr := []string{"a", "b", "c"}
	arr2 := Strings(arr)

	// within bounds
	fmt.Println(arr2.Z(1))
	fmt.Println(arr2.B(1))
	fmt.Println(arr2.E(1))

	// out of bounds
	fmt.Println(arr2.Z(4))
	fmt.Println(arr2.B(4))
	fmt.Println(arr2.E(4))

	// Output:
	// b
	// b true
	// b <nil>
	// 
	//  false
	//  Index 4 out of bounds.
}

func ExampleInts() {
	arr := []int{0, 1, 2}
	arr2 := Ints(arr)

	// within bounds
	fmt.Println(arr2.Z(1))
	fmt.Println(arr2.B(1))
	fmt.Println(arr2.E(1))

	// out of bounds
	fmt.Println(arr2.Z(4))
	fmt.Println(arr2.B(4))
	fmt.Println(arr2.E(4))

	// Output:
	// 1
	// 1 true
	// 1 <nil>
	// 0
	// 0 false
	// 0 Index 4 out of bounds.
}

// Package oob experiments with ways to avoid array out of bounds panics.
// At the time of writing, my main use case for these patterns was to check for presence of optional arguments provided by the user via os.Args. I've since discovered flag.Arg(i) which relieves the need for checking array length before accessing the element.
// If accessing an array where the index may be out of bounds, consider using a map[int]T instead.
package oob

import (
	"fmt"
)

type OutOfBoundsError struct{ i int }

func (e *OutOfBoundsError) Error() string {
	return fmt.Sprintf("Index %v out of bounds.", e.i)
}

// MapStringArr converts an array of strings to an int indexed map of strings. 
// If an element is requested that doesn't exist, the map will return the zero value (empty string).
// Existance of an element can be checked for by:
// 	if v, ok := m[i]; ok {
// 		// m[i] exists
// 	} else {
// 		// m[i] is out of bounds, v is ""
// 	}
func MapStringArr(a []string) map[int]string {

	m := make(map[int]string)

	for i, v := range a {
		m[i] = v
	}

	return m
}

// type DocArray is only used to document the behaviour of methods B, Z and E on other array types.
// oob defines a series of types of arrays of basic types which implement methods B, Z and E.
// Z(i) returns the ith value of the array, or the [zero value](http://tour.golang.org/basics/12) of the underlying array element type, if i is out of bounds of the array.
// B(i) acts like Z but returns also returns a bool indicating whether i is within bounds.
// E(i) acts like Z but also returns an error indicating whether the i is out of bounds.
type DocArray []int

// Strings an array of string. For method behaviour see DocArray.
type Strings []string

func (a Strings) B(i int) (string, bool) {
	// zero value for this type:
	var zero string

	if len(a) < i {
		return zero, false
	} else {
		return a[i], true
	}
}

func (a Strings) E(i int) (string, error) {

	if o, ok := a.B(i); ok {
		return o, nil
	} else {
		return o, &OutOfBoundsError{i: i}

	}
}

func (a Strings) Z(i int) string {

	o, _ := a.E(i)
	return o
}

// Ints an array of int. For method behaviour see DocArray.
type Ints []int

func (a Ints) B(i int) (int, bool) {
	// zero value for this type:
	var zero int

	if len(a) < i {
		return zero, false
	} else {
		return a[i], true
	}
}

func (a Ints) E(i int) (int, error) {

	if o, ok := a.B(i); ok {
		return o, nil
	} else {
		return o, &OutOfBoundsError{i: i}

	}
}

func (a Ints) Z(i int) int {

	o, _ := a.E(i)
	return o
}
